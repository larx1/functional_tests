package org.larx1.qa.functional.utils;

import org.apache.commons.codec.digest.DigestUtils;

import java.util.Arrays;
import java.util.Base64;

public class HexEncoding {

    public static void main(String a[]) {
        System.out.println("Encoding: " + hexaDecimalEncodingSHA256("5546537951638"));
    }

    public static String hexaDecimalEncodingSHA256(String str) {
        final String hash = DigestUtils.sha256Hex(str);
        return hash;
    }

    public static String hexaDecimalEncodingSHA384(String str) {
        final String hash = DigestUtils.sha384Hex(str);
        return hash;
    }

    public static String hexaDecimalEncodingSHA512(String str) {
        final String hash = DigestUtils.sha512Hex(str);
        return hash;
    }

    public static String hexaDecimalEncodingSHA1Hex(String str) {
        final String hash = DigestUtils.sha1Hex(str);
        return hash;
    }

    public static String encode64(String tmnId, String phoneNumber, String tHash) {
        String secret = String.join("|", Arrays.asList(tmnId, phoneNumber, tHash));
        return Base64.getEncoder().encodeToString(secret.getBytes());
    }

}