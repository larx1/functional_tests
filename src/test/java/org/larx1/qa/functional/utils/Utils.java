package org.larx1.qa.functional.utils;

import java.util.Random;

public class Utils {

    public static void main(String a[]) {
        String thaiId = getRandomThaiId();
        System.out.println("Thaid id = " + thaiId);
        System.out.println("Is valid ? " + isThaiIdValid(thaiId));
        System.out.println("Makro Id = " + getRandomMakroId());
        String tmnId = getRandomTmnId();
        System.out.println("Tmn Id = " + tmnId);
        String mobileNo = "0123456789";
        System.out.println("Mobile no = " + mobileNo);
        String thaiIdHash = HexEncoding.hexaDecimalEncodingSHA256(thaiId);
        System.out.println("Thai Id Hash " + thaiIdHash);
        System.out.println("Auth header = " + HexEncoding.hexaDecimalEncodingSHA256(tmnId + "|" + mobileNo + "|" + thaiIdHash));
    }

    public static String getRandomMakroId() {
        return "3000" + randomNumberInRange(100000000, 999999999);
    }

    public static String getRandomTmnId() {
        return "" + randomNumberInRange(10000, 99999);
    }

    public static String getRandomThaiId() {
        String thaiId = null;
        do {
            thaiId = generateRandomThaiId();
        } while(!isThaiIdValid(thaiId));
        return thaiId;
    }

    private static String generateRandomThaiId() {
        StringBuilder builder = new StringBuilder();
        Random random = new Random();

        int x = random.nextInt(8) + 1;
        int sum = x * 13;
        builder.append(x);

        for (int i = 1; i < 12; i++) {
            x = random.nextInt(10);
            sum += x * (13 - i);
            builder.append(x);
        }

        int checksum = (11 - sum % 11) % 10;
        builder.append(checksum);
        return builder.toString();
    }

    public static boolean isThaiIdValid(String idCard){
        if (idCard.length()!=13){
            return false;
        }
        int sum = 0;
        for (int i = 0; i < 12; i++) {
            sum += Character.getNumericValue(idCard.charAt(i))*(13-i);
        }
        return (11-sum%11)%10 == Character.getNumericValue(idCard.charAt(12));
    }


    public static int randomNumberInRange(int min, int max) {
        Random random = new Random();
        return random.nextInt((max - min) + 1) + min;
    }

}
