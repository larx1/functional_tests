package org.larx1.qa.functional.utils;

import io.qameta.allure.Attachment;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class Allure {

    public static class Attachments {

        @Attachment(value = "Screenshot", type = "image/png")
        public static byte[] screenshot(byte[] screenShot) {
            return screenShot;
        }

        @Attachment(value = "Json schema", type = "text/json")
        public static byte[] jsonSchema(String jsonPath) {
            byte[] file = null;
            try {
                ClassLoader classLoader = Attachments.class.getClassLoader();
                file = Files.readAllBytes(new File(classLoader.getResource(jsonPath).getFile()).toPath());
            } catch (IOException exception) {
                exception.printStackTrace();
            }
            return file;
        }

    }

}
