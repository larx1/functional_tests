package org.larx1.qa.functional.config;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;
import org.larx1.qa.functional.config.managers.PageObjectManager;
import org.larx1.qa.functional.config.managers.WebDriverManager;
import org.larx1.qa.functional.filters.AllureRestAssured;
import org.larx1.qa.functional.model.api.json.testrail.AddResultForCaseRequestBody;
import org.larx1.qa.functional.utils.Allure;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.util.Collection;

import static io.restassured.RestAssured.given;

public class Hooks {

    private static final String BASE_URI = "https://sit.acp.wize.mx/credit-pay-be/api/";
    private static final String TEST_RAIL_URL_ADD_RESULT_FOR_CASE = "https://wizeline.testrail.net/index.php?/api/v2/add_result_for_case/{runId}/{caseId}";
    private static final String TEST_RAIL_USERNAME = "roberto.oropeza@wizeline.com";
    private static final String TEST_RAIL_API_KEY = "2mhjR8SVNNdCaa6sZc9q-HsV0oUAkj4UkFA4..PC.";
    private static final String EMPTY_STRING = "";
    private static final String TEST_RAIL_RUN_ID = "89";

    private TestContext testContext;

    public Hooks(TestContext testContext) {
        this.testContext = testContext;
    }

    @Before("@api")
    public void beforeApi() {
        RestAssured.baseURI = BASE_URI;
        if (RestAssured.filters().size() == 0) {
            RestAssured.filters(new AllureRestAssured());
        }
    }

    @Before("@e2e")
    public void beforeE2E() {
        testContext.setWebDriverManager(new WebDriverManager());
        testContext.setPageObjectManager(new PageObjectManager(testContext.getWebDriverManager().getDriver()));
    }

    @After("@e2e")
    public void afterE2E() {
        final byte[] screenshot = ((TakesScreenshot) testContext.getWebDriverManager().getDriver()).getScreenshotAs(OutputType.BYTES);
        Allure.Attachments.screenshot(screenshot);
        testContext.getWebDriverManager().close();
    }

    //Running this method at the end of all the hooks.
    @After(order = 0)
    public void updateTestRail(Scenario scenario) {
        Collection<String> tags = scenario.getSourceTagNames();
        String testRailId = null;
        String defectId = null;
        for (String tag : tags) {
            if(tag.startsWith("@issue=")) {
                defectId = tag.startsWith("@issue=") ? tag.split("=")[1] : null;
                continue;
            }
            if(tag.startsWith("@tmsLink=")) {
                testRailId = tag.startsWith("@tmsLink=") ? tag.split("=")[1] : null;
                continue;
            }
        }
        if (testRailId != null) {
            //Step 1 : Attach the result to the case id
            Integer statusId = scenario.isFailed() ? 5 : 1;
            //Create the json request
            AddResultForCaseRequestBody addResultForCaseRequestBody
                    = new AddResultForCaseRequestBody(statusId, EMPTY_STRING, EMPTY_STRING, defectId, EMPTY_STRING);
            //Send the result to Test Rail
            given()
                    .body(addResultForCaseRequestBody, ObjectMapperType.JACKSON_2)
                    .contentType(ContentType.JSON)
                    .urlEncodingEnabled(false)
                    .auth().preemptive()
                    .basic(TEST_RAIL_USERNAME, TEST_RAIL_API_KEY)
                    .expect().statusCode(200)
                    .when().post(TEST_RAIL_URL_ADD_RESULT_FOR_CASE, TEST_RAIL_RUN_ID, testRailId);
        }
    }

}
