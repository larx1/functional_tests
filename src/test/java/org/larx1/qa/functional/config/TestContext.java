package org.larx1.qa.functional.config;

import io.restassured.http.Header;
import io.restassured.response.Response;
import org.larx1.qa.functional.config.managers.PageObjectManager;
import org.larx1.qa.functional.config.managers.WebDriverManager;
import org.larx1.qa.functional.model.api.json.RequestBody;

public class TestContext {

    private RequestBody requestBody;
    private Response response;
    private WebDriverManager webDriverManager;
    private PageObjectManager pageObjectManager;
    private Header authorizationHeader;
    private String thaiId;
    private String tmnId;
    private String thaiIdHash;

    public Response getResponse() {
        return this.response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public PageObjectManager getPageObjectManager() {
        return pageObjectManager;
    }

    public void setPageObjectManager(PageObjectManager pageObjectManager) {
        this.pageObjectManager = pageObjectManager;
    }

    public WebDriverManager getWebDriverManager() {
        return webDriverManager;
    }

    public void setWebDriverManager(WebDriverManager webDriverManager) {
        this.webDriverManager = webDriverManager;
    }

    public Header getAuthorizationHeader() {
        return authorizationHeader;
    }

    public void setAuthorizationHeader(Header authorizationHeader) {
        this.authorizationHeader = authorizationHeader;
    }

    public RequestBody getRequestBody() {
        return requestBody;
    }

    public void setRequestBody(RequestBody requestBody) {
        this.requestBody = requestBody;
    }

    public String getThaiId() {
        return thaiId;
    }

    public void setThaiId(String thaiId) {
        this.thaiId = thaiId;
    }

    public String getTmnId() {
        return tmnId;
    }

    public void setTmnId(String tmnId) {
        this.tmnId = tmnId;
    }

    public String getThaiIdHash() {
        return thaiIdHash;
    }

    public void setThaiIdHash(String thaiIdHash) {
        this.thaiIdHash = thaiIdHash;
    }
}
