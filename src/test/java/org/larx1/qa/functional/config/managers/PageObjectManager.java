package org.larx1.qa.functional.config.managers;

import org.larx1.qa.functional.model.e2e.pages.WelcomePage;
import org.openqa.selenium.WebDriver;

public class PageObjectManager {

    private WebDriver driver;

    private WelcomePage welcomePage;

    public PageObjectManager(WebDriver driver) {
        this.driver = driver;
    }

    public WelcomePage getWelcomePage() {
        return (welcomePage == null) ? welcomePage = new WelcomePage(driver) : welcomePage;
    }

}
