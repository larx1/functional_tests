package org.larx1.qa.functional.steps.api.lite;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import org.larx1.qa.functional.config.TestContext;
import org.larx1.qa.functional.steps.api.APISteps;

import static io.restassured.RestAssured.given;

public class BalanceControllerGetSteps extends APISteps {

    public BalanceControllerGetSteps(TestContext testContext) {
        super(testContext, "/lite/v1/balance/credit-pay");
    }

    @Given("^I get the balance using /api/lite/v1/balance GET$")
    public void i_get_the_balance_using_api_lite_v_balance_GET() {
        testContext.setResponse(given()
                .header(testContext.getAuthorizationHeader())
                .when()
                .get(uri));
    }

    @Then("^validate the balance using /api/lite/v1/balance GET is (.*)$")
    public void validate_the_balance_using_api_lite_v1_balance_GET_is(String balanceArg) {
        Float balance = testContext.getResponse().then().extract().path("data.balance");
        Assert.assertEquals(balance.toString(), balanceArg);
    }

}
