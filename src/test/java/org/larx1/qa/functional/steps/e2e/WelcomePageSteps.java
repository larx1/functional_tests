package org.larx1.qa.functional.steps.e2e;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import org.larx1.qa.functional.config.TestContext;
import org.larx1.qa.functional.steps.e2e.E2ESteps;

public class WelcomePageSteps extends E2ESteps {

    public WelcomePageSteps(TestContext testContext) {
        super(testContext);
    }

    @Given("^Im on the welcome page$")
    public void im_on_the_welcome_page() {
        testContext.getPageObjectManager().getWelcomePage();
    }

    @Then("^The register button should be displayed$")
    public void the_register_button_should_be_displayed() {
        Assert.assertTrue(testContext.getPageObjectManager().getWelcomePage().isRegisterButtonDisplayed());
    }

}
