package org.larx1.qa.functional.steps.api;

import io.restassured.http.Header;
import org.larx1.qa.functional.config.TestContext;
import org.larx1.qa.functional.steps.Steps;
import org.larx1.qa.functional.utils.HexEncoding;

public class APISteps extends Steps {

    protected String uri;

    public APISteps(TestContext testContext, String uri) {
        super(testContext);
        this.uri = uri;
    }

    public Header getAuthorizationHeader(String tmnId, String mobileNo, String thaiIdHash) {
        String newHash = HexEncoding.encode64(tmnId, mobileNo, thaiIdHash);
        return new Header("Authorization", "Bearer " + newHash);
    }

    public Header getExternalAuthorizationHeader() {
        return new Header("Authorization", "Basic VENSQjpkdW1teQo=");
    }

}
