package org.larx1.qa.functional.steps.api;

import cucumber.api.java.en.Then;
import io.restassured.module.jsv.JsonSchemaValidator;
import org.larx1.qa.functional.config.TestContext;
import org.larx1.qa.functional.utils.Allure;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.core.IsEqual.equalTo;

public class CommonApiSteps extends APISteps {

    public CommonApiSteps(TestContext testContext) {
        super(testContext, null);
    }

    @Then("^the status code should be \"([^\"]*)\"$")
    public void the_status_code_should_be(String statusCode) {
        testContext.getResponse().then().statusCode(Integer.parseInt(statusCode));
    }

    @Then("^the response should return unauthorized error$")
    public void the_response_should_return_unauthorized_error() {
        Map<String, String> errorMessage = new HashMap<String, String>();
        errorMessage.put("errMessage", "Unauthorized request");
        errorMessage.put("errCode", "TOKEN_NOT_PROVIDED");
        the_error_message_should_contains_the_following_description(errorMessage);
    }

    @Then("^the error message should contains the following description$")
    public void the_error_message_should_contains_the_following_description(Map<String, String> map) {
        testContext.getResponse().then().body(
                "errMessage", equalTo(map.get("errMessage")),
                "errCode", equalTo(map.get("errCode"))
        );
    }

    @Then("^validate the response according the schema \"([^\"]*)\"$")
    public void validate_the_response_according_the_schema(String schemaFile) {
        Allure.Attachments.jsonSchema(schemaFile);
        testContext.getResponse().then().assertThat().body(
                JsonSchemaValidator.matchesJsonSchemaInClasspath(schemaFile));
    }

}
