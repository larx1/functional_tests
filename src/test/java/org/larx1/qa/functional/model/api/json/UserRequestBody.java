package org.larx1.qa.functional.model.api.json;

public class UserRequestBody {

    private Boolean consentFlag;
    private String makroId;
    private String selfDeclare;
    private String tmnId;

    public UserRequestBody(Boolean consentFlag, String makroId, String selfDeclare, String tmnId) {
        this.consentFlag = consentFlag;
        this.makroId = makroId;
        this.selfDeclare = selfDeclare;
        this.tmnId = tmnId;
    }

    public Boolean getConsentFlag() {
        return consentFlag;
    }

    public void setConsentFlag(Boolean consentFlag) {
        this.consentFlag = consentFlag;
    }

    public String getMakroId() {
        return makroId;
    }

    public void setMakroId(String makroId) {
        this.makroId = makroId;
    }

    public String getSelfDeclare() {
        return selfDeclare;
    }

    public void setSelfDeclare(String selfDeclare) {
        this.selfDeclare = selfDeclare;
    }

    public String getTmnId() {
        return tmnId;
    }

    public void setTmnId(String tmnId) {
        this.tmnId = tmnId;
    }
}
