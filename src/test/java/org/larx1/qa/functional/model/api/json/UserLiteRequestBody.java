package org.larx1.qa.functional.model.api.json;

public class UserLiteRequestBody extends RequestBody {

    private Boolean consentFlag;
    private Integer kycLevel;
    private String makroId;
    private String phoneNumber;
    private String selfDeclare;
    private String thaiId;
    private String tmnId;
    private String email;

    public UserLiteRequestBody(Boolean consentFlag, Integer kycLevel, String makroId, String phoneNumber, String selfDeclare, String thaiId, String tmnId, String email) {
        this.consentFlag = consentFlag;
        this.kycLevel = kycLevel;
        this.makroId = makroId;
        this.phoneNumber = phoneNumber;
        this.selfDeclare = selfDeclare;
        this.thaiId = thaiId;
        this.tmnId = tmnId;
        this.email = email;
    }

    public Boolean getConsentFlag() {
        return consentFlag;
    }

    public void setConsentFlag(Boolean consentFlag) {
        this.consentFlag = consentFlag;
    }

    public Integer getKycLevel() {
        return kycLevel;
    }

    public void setKycLevel(Integer kycLevel) {
        this.kycLevel = kycLevel;
    }

    public String getMakroId() {
        return makroId;
    }

    public void setMakroId(String makroId) {
        this.makroId = makroId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getSelfDeclare() {
        return selfDeclare;
    }

    public void setSelfDeclare(String selfDeclare) {
        this.selfDeclare = selfDeclare;
    }

    public String getThaiId() {
        return thaiId;
    }

    public void setThaiId(String thaiId) {
        this.thaiId = thaiId;
    }

    public String getTmnId() {
        return tmnId;
    }

    public void setTmnId(String tmnId) {
        this.tmnId = tmnId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
