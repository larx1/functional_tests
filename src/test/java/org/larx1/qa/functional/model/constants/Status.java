package org.larx1.qa.functional.model.constants;

public enum Status {

    Approved("00"),
    Rejected("20");

    private String status;

    private Status(String statusArg) {
        this.status = statusArg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
