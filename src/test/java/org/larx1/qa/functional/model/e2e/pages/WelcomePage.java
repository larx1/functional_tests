package org.larx1.qa.functional.model.e2e.pages;

import org.larx1.qa.functional.model.e2e.pages.GenericPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WelcomePage extends GenericPage {

    @FindBy(xpath = "//button[text()='Register']")
    private WebElement buttonRegister;

    public WelcomePage(WebDriver driver) {
        super(driver);
    }

    public boolean isRegisterButtonDisplayed() {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.visibilityOf(buttonRegister));
        return buttonRegister.isDisplayed();
    }

}
