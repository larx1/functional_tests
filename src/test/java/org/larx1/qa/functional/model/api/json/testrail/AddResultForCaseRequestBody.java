package org.larx1.qa.functional.model.api.json.testrail;

public class AddResultForCaseRequestBody {

    private Integer status_id;
    private String comment;
    private String elapsed;
    private String defects;
    private String version;

    public AddResultForCaseRequestBody(Integer status_id, String comment, String elapsed, String defects, String version) {
        this.status_id = status_id;
        this.comment = comment;
        this.elapsed = elapsed;
        this.defects = defects;
        this.version = version;
    }

    public Integer getStatus_id() {
        return status_id;
    }

    public void setStatus_id(Integer status_id) {
        this.status_id = status_id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getElapsed() {
        return elapsed;
    }

    public void setElapsed(String elapsed) {
        this.elapsed = elapsed;
    }

    public String getDefects() {
        return defects;
    }

    public void setDefects(String defects) {
        this.defects = defects;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
