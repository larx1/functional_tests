@e2e
@disabled
Feature: E2E - Welcome page
<p>
    <img src="https://img.shields.io/badge/author-Roberto-green.svg">
</p>

    @severity=critical
    Scenario: Register button
    Given Im on the welcome page
    Then The register button should be displayed
