@api
Feature: /api/lite/v1/users POST
<p>
    <img src="https://img.shields.io/badge/author-Roberto-green.svg">
</p>

    @severity=blocker
    Scenario: /api/lite/v1/users POST 201 - Create a user
    Given I create a user using /lite/v1/users POST
    Then the status code should be "201"

    @severity=critical
    Scenario: /api/lite/v1/users POST 403 - Try to create an already existing user
    Given I create a user using /lite/v1/users POST
    Then I create a user using /lite/v1/users POST with an already used request
    And the status code should be "403"
    And validate the response according the schema "error_ALL_schema.json"
